﻿using hw7;
using System.Diagnostics;

TestSorting(100);
TestSorting(1000);
TestSorting(10000);
TestSorting(100000);
Console.ReadLine();

void TestSorting(int arraySize)
{
    Console.WriteLine($"Array size = {arraySize}.");
    var testData1 = RandomDataGenerator.GenerateRandomArray(arraySize);
    var testData2 = testData1.ToArray(); // Cloning testData1 array
    var testData3 = testData1.ToArray(); // Cloning testData1 array

    var sw = Stopwatch.StartNew();
    testData1.SelectionSort();
    sw.Stop();

    // Testing whether or no sorting is correct.
    if (!testData1.ToArray().SequenceEqual(testData1.OrderBy(i => i))) throw new Exception("There's an error in the selection sorting algorithm!");

    Console.WriteLine($"Selection:\t{sw.ElapsedTicks} ticks ~ {sw.ElapsedMilliseconds} ms");

    sw = Stopwatch.StartNew();
    testData2.HeapSort();
    sw.Stop();

    // Testing whether or no sorting is correct.
    if (!testData2.ToArray().SequenceEqual(testData2.OrderBy(i => i))) throw new Exception("There's an error in the heap sorting algorithm!");

    Console.WriteLine($"Heap:\t\t{sw.ElapsedTicks} ticks ~ {sw.ElapsedMilliseconds} ms");

    Console.WriteLine("----------------------------------------------");
}
