﻿namespace hw7;

public static class RandomDataGenerator
{
    public static IList<long> GenerateRandomArray(long arraySize)
    {
        long[] result = new long[arraySize];
        Random random = new();
        for (int i = 0; i < result.Length; i++)
        {
            long temp = random.NextInt64(-40, 40);
            result[i] = temp;
        }

        return result;
    }
}