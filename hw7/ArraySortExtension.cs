﻿namespace hw7;

public static class ArraySortExtension
{
    public static void SelectionSort<T>(this IList<T> collection)
        where T : struct, IComparable<T>
    {
        int max = collection.MaxIndex(collection.Count);
        for (int i = collection.Count - 1; i > 0; i--)
        {
            if (max != i) collection.Swap(i, max);

            max = MaxIndex(collection, i);
        }
    }

    private static int MaxIndex<T>(this IList<T> collection, int size)
        where T : IComparable<T>
    {
        int max = 0;
        for (int i = 0; i < size; i++)
            if (collection[max].CompareTo(collection[i]) == -1) max = i;

        return max;
    }

    public static void HeapSort<T>(this IList<T> collection)
        where T : IComparable<T>
    {
        for (int root = collection.Count / 2 - 1; root >= 0; root--)
            collection.Heapify(root, collection.Count);

        for (int i = collection.Count - 1; i > 0; i--)
        {
            collection.Swap(0, i);
            collection.Heapify(0, i);
        }
    }

    private static void Heapify<T>(this IList<T> collection, int root, int size)
        where T : IComparable<T>
    {
        int l = 2 * root + 1;
        int r = 2 * root + 2;
        int x = root;
        if (l < size && collection[l].CompareTo(collection[x]) == 1) x = l;
        if (r < size && collection[r].CompareTo(collection[x]) == 1) x = r;

        if (x == root) return;

        collection.Swap(x, root);
        collection.Heapify(x, size);
    }

    private static void Swap<T>(this IList<T> collection, int index1, int index2)
    {
        (collection[index2], collection[index1]) = (collection[index1], collection[index2]);
    }
}