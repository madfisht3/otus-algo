﻿using hw14;

namespace hw15
{
    public static class GraphDemucronExtension
    {
        public static int[][] Demoucron(this Graph graph)
        {
            var vertices = graph.Vertices;
            var result = new List<int[]>();

            int[] summs = new int[vertices.GetLength(1)];

            // Суммируем столбцы матрицы смежности
            for (int i = 0; i < vertices.GetLength(0); i++)
            {
                for (int j = 0; j < summs.Length; j++)
                {
                    if (!string.IsNullOrEmpty(vertices[i, j])) summs[i]++;
                }
            }

            // Формируем результат вида: массив { [ [уровень], [1-я вершина], ..., [n-я вершина] ] }
            int level = 1;
            var part = new int[summs.Length + 1];
            while (!summs.All(i => i == 0))
            {
                part[0] = level;
                Array.Copy(summs, 0, part, 1, summs.Length);

                for (int i = 1; i < part.Length; i++)
                {
                    if (part[i] > 0) summs[i - 1]--;

                    if (part[i] != 1) part[i] = 0;
                }

                result.Add(part.ToArray());
                level++;
            }


            return result.ToArray();
        }
    }
}
