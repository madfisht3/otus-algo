﻿using hw14;
using hw15;

var graph = GraphLoader.LoadFromFile("grapth1.graph");

var vertices = graph.Vertices;

Console.WriteLine("The original graph:\r\n--------------------------------------------------");

Console.Write("\t");
for (int i = 1; i <= vertices.GetLength(0); i++)
{
    Console.Write($"{i}\t");
}
Console.WriteLine();

for (int i = 0; i < vertices.GetLength(1); i++)
{
    Console.Write($"{i + 1}\t");

    for (int j = 0; j < vertices.GetLength(1); j++)
    {
        Console.Write($"{vertices[i,j]}\t");
    }
    Console.WriteLine();
}

Console.WriteLine("--------------------------------------------------");

Console.WriteLine("Demoucron algorithm levels:");

Console.WriteLine("--------------------------------------------------");

var result = graph.Demoucron();
Console.Write("\t");
for (int i = 1; i < result[0].Length; i++)
{
    Console.Write($"{i}\t");
}
Console.WriteLine();

foreach (var level in result)
{
    for (int i = 0; i < level.Length; i++)
    {
        Console.Write($"{level[i]}\t");
    }
    Console.WriteLine();
}
Console.WriteLine("--------------------------------------------------");