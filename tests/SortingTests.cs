﻿using System.Linq;
using Xunit;
using hw6;
using hw7;
using RandomDataGenerator = Tests.data.Generators.RandomDataGenerator;
using HW9RandomDataGenerator = hw9.RandomDataGenerator;
using FluentAssertions;
using hw8;
using hw9;

namespace Tests
{
    public class SortingTests
    {
        [Fact]
        public void BubbleSortTest()
        {
            // Arrange
            var dataToSort = RandomDataGenerator.GenerateRandomArray(1000);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.BubbleSort();

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void InsertionSortTest()
        {
            // Arrange
            var dataToSort = RandomDataGenerator.GenerateRandomArray(1000);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.InsertionSort();

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void ShellSortTest()
        {
            // Arrange
            var dataToSort = RandomDataGenerator.GenerateRandomArray(1000);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.ShellSort();

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void SelectionSortTest()
        {
            // Arrange
            var dataToSort = RandomDataGenerator.GenerateRandomArray(1000);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.SelectionSort();

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void HeapSortTest()
        {
            // Arrange
            var dataToSort = RandomDataGenerator.GenerateRandomArray(1000);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.HeapSort();

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void QuickSort1Test()
        {
            // Arrange
            var dataToSort = RandomDataGenerator.GenerateRandomArray(1000);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.QuickSort1();

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void QuickSort2Test()
        {
            // Arrange
            var dataToSort = RandomDataGenerator.GenerateRandomArray(1000);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.QuickSort2();

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void MergeSortTest()
        {
            // Arrange
            var dataToSort = RandomDataGenerator.GenerateRandomArray(1000);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.MergeSort();

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void CountingSortTest()
        {
            // Arrange
            const int max = 999;
            var dataToSort = HW9RandomDataGenerator.GenerateArray(1000000, 0, max);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.CountingSort(max);

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void BucketSortTest()
        {
            // Arrange
            const int max = 999;
            var dataToSort = HW9RandomDataGenerator.GenerateArray(1000000, 0, max);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.BucketSort(max);

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }

        [Fact]
        public void RadixSortTest()
        {
            // Arrange
            const int max = 999;
            var dataToSort = HW9RandomDataGenerator.GenerateArray(1000000, 0, max);
            var dataToTest = dataToSort.OrderBy(i => i).ToArray();

            // Act
            dataToSort.RadixSort();

            // Assert
            dataToSort.SequenceEqual(dataToTest).Should().BeTrue();
        }
    }
}
