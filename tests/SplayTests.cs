﻿//using FluentAssertions;
//using hw11;
//using Xunit;

//namespace Tests
//{
//    public class SplayTests
//    {
//        [Fact]
//        public void Search_Test()
//        {
//            //Arrange
//            var tree = new SplayTree();

//            tree.Insert(1);
//            tree.Insert(2);
//            tree.Insert(3);
//            tree.Insert(4);

//            //Act
//            tree.Search(4);
//            tree.Search(1);

//            //Assert
//            tree.Root.Key.Should().Be(1);
//            tree.Root.Right.Key.Should().Be(2);
//            tree.Root.Right.Right.Key.Should().Be(4);
//            tree.Root.Right.Right.Left.Key.Should().Be(3);
//        }

//        [Fact]
//        public void Insert_Test()
//        {
//            //Arrange
//            var tree = new SplayTree();

//            //Act
//            tree.Insert(1);
//            tree.Insert(2);
//            tree.Insert(3);
//            tree.Insert(4);

//            //Assert
//            tree.Root.Key.Should().Be(1);
//            tree.Root.Right.Key.Should().Be(2);
//            tree.Root.Right.Right.Key.Should().Be(3);
//            tree.Root.Right.Right.Right.Key.Should().Be(4);
//        }
//    }
//}
