﻿using hw2;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace Tests
{
    public class StringLengthCalculatorTests
    {
        [Theory]
        [ClassData(typeof(Case1Data))]
        public void GetStringLengthNaiveTest(string input, long output)
        {
            IStringLengthCalculator stringLengthCalculator = new NaiveStringLengthCalculator();

            long actual = stringLengthCalculator.CalculateLength(input);

            Assert.Equal(output, actual);
        }
    }

    public class Case1Data : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            string directory = Path.Combine("data", "0.String");
            for (int i = 0; i < int.MaxValue; i++)
            {
                string inputFilename = Path.Combine(directory, $"test.{i}.in");
                string outputFilename = Path.Combine(directory, $"test.{i}.out");
                if (!File.Exists(inputFilename)
                    || !File.Exists(outputFilename))
                {
                    yield break;
                }

                string input = File.ReadAllLines(inputFilename).First().Trim();
                string output = File.ReadAllLines(outputFilename).First(l => !string.IsNullOrEmpty(l)).Trim();

                yield return new object[] { input, long.Parse(output) };
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}