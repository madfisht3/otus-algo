﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace Tests
{
    public class KingMoveCalculatorTests
    {
        [Theory]
        [ClassData(typeof(KingData))]
        public void CalculateTest(int position, int count, ulong mask)
        {
            KingMoveCalulator kingMoveCalulator = new();

            var actual = kingMoveCalulator.Calculate(1UL << position);

            Assert.Equal((count, mask), actual);
        }
    }

    public class KingData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            string directory = Path.Combine("data", "0.BITS", "1.Bitboard - Король");
            for (int i = 0; i < int.MaxValue; i++)
            {
                string inputFilename = Path.Combine(directory, $"test.{i}.in");
                string outputFilename = Path.Combine(directory, $"test.{i}.out");
                if (!File.Exists(inputFilename)
                    || !File.Exists(outputFilename))
                {
                    yield break;
                }

                string input = File.ReadAllLines(inputFilename).First().Trim();

                string[] output = File.ReadAllLines(outputFilename).Where(l => !string.IsNullOrEmpty(l)).ToArray();

                yield return new object[] { int.Parse(input), int.Parse(output[0]), ulong.Parse(output[1]) };
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}