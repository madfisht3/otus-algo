﻿using hw4;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace Tests
{
    public class KnightMoveCalculatorTests
    {
        [Theory]
        [ClassData(typeof(KnightData))]
        public void CalculateTest(int position, int count, ulong mask)
        {
            KnightMoveCalulator knightMoveCalulator = new();

            var actual = knightMoveCalulator.Calculate(1UL << position);

            Assert.Equal((count, mask), actual);
        }
    }

    public class KnightData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            string directory = Path.Combine("data", "0.BITS", "2.Bitboard - Конь");
            for (int i = 0; i < int.MaxValue; i++)
            {
                string inputFilename = Path.Combine(directory, $"test.{i}.in");
                string outputFilename = Path.Combine(directory, $"test.{i}.out");
                if (!File.Exists(inputFilename)
                    || !File.Exists(outputFilename))
                {
                    yield break;
                }

                string input = File.ReadAllLines(inputFilename).First().Trim();

                string[] output = File.ReadAllLines(outputFilename).Where(l => !string.IsNullOrEmpty(l)).ToArray();

                yield return new object[] { int.Parse(input), int.Parse(output[0]), ulong.Parse(output[1]) };
            }
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
