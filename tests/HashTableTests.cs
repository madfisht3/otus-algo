﻿using FluentAssertions;
using hw12;
using System;
using System.Collections.Generic;
using Xunit;

namespace Tests
{
    public abstract class HashTableTests<T> where T : IHashTable<int, int>
    {
        protected abstract IHashTable<int, int> MakeAnInstance();

        [Fact]
        public void ChainHT_Add_AddOne_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            hashTable.Add(1, 2);

            //Assert
            hashTable[1].Should().Be(2);
            hashTable.Size.Should().Be(1);
        }

        [Fact]
        public void ChainHT_Add_AddTwelve_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            for (int i = 0; i < 12; i++)
                hashTable.Add(i, i + 1);

            //Assert
            for (int i = 0; i < 12; i++)
                hashTable[i].Should().Be(i + 1);

            hashTable.Size.Should().Be(12);
        }

        [Fact]
        public void ChainHT_Add_AddMillion_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            for (int i = 0; i < 1000000; i++)
                hashTable.Add(i, i + 1);

            //Assert
            for (int i = 0; i < 1000000; i++)
                hashTable[i].Should().Be(i + 1);

            hashTable.Size.Should().Be(1000000);
        }

        [Fact]
        public void ChainHT_ContainsKey_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            hashTable.Add(15, 40);

            //Assert
            hashTable.ContainsKey(15).Should().BeTrue();
        }

        [Fact]
        public void ChainHT_ContainsKey_AfterRehash_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            for (int i = 0; i < 12; i++)
                hashTable.Add(i, i + 1);

            //Assert
            hashTable.ContainsKey(11).Should().BeTrue();
        }

        [Fact]
        public void ChainHT_ContainsValue_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            hashTable.Add(1000, 1);

            //Assert
            hashTable.ContainsValue(1).Should().BeTrue();
        }

        [Fact]
        public void ChainHT_ContainsValue_AfterRehash_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            for (int i = 0; i < 12; i++)
                hashTable.Add(i, i + 1);

            //Assert
            hashTable.ContainsValue(11).Should().BeTrue();
        }

        [Fact]
        public void ChainHT_Remove_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            hashTable.Add(1000, 1);
            hashTable.Remove(1000);

            //Assert
            hashTable.ContainsKey(1000).Should().BeFalse();
            hashTable.ContainsValue(1).Should().BeFalse();
        }

        [Fact]
        public void ChainHT_Remove_AfterRehash_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            for (int i = 0; i < 5; i++)
                hashTable.Add(i, i + 1);
            
            hashTable.Remove(3);

            for (int i = 5; i < 13; i++)
                hashTable.Add(i, i + 1);


            //Assert
            hashTable.ContainsKey(3).Should().BeFalse();
            hashTable.ContainsValue(4).Should().BeFalse();
        }

        [Fact]
        public void ChainHT_Get_NegativeTest()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            for (int i = 0; i < 12; i++)
                hashTable.Add(i, i + 1);

            Action act = () =>
            {
                int test = hashTable[15];
            };

            //Assert
            act.Should().Throw<KeyNotFoundException>()
                .WithMessage("15");
        }

        [Fact]
        public void ChainHT_Get_AfterRehash_NegativeTest()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            for (int i = 0; i < 12; i++)
                hashTable.Add(i, i + 1);

            Action act = () =>
            {
                int test = hashTable[95];
            };

            //Assert
            act.Should().Throw<KeyNotFoundException>()
                .WithMessage("95");
        }

        [Fact]
        public void ChainHT_Set_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();
            hashTable.Add(10, 109);

            //Act
            hashTable[10] = 500;

            //Assert
            hashTable.ContainsKey(10).Should().BeTrue();
            hashTable.ContainsValue(500).Should().BeTrue();
        }

        [Fact]
        public void ChainHT_Set_AfterRehash_Test()
        {
            //Arrange
            var hashTable = MakeAnInstance();

            //Act
            for (int i = 0; i < 12; i++)
                hashTable.Add(i, i + 1);

            hashTable[5] = 1000;

            //Assert
            hashTable.ContainsKey(5).Should().BeTrue();
            hashTable.ContainsValue(1000).Should().BeTrue();
        }
    }
}
