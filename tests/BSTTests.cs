﻿using FluentAssertions;
using hw10;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tests
{
    public class BSTTests
    {
        [Fact]
        public void BST_InsertTest()
        {
            // Arrange
            BST tree = new();

            //Act
            tree.Insert(30);
            tree.Insert(10);
            tree.Insert(15);
            tree.Insert(20);

            tree.Insert(40);
            tree.Insert(35);
            tree.Insert(34);
            tree.Insert(41);

            // It must be:
            /*   
                  _____30_____
                /             \
              10              40
                \            /  \
                 15        35    41
                   \      /
                    20  34             */
            //Assert
            tree.Root.Key.Should().Be(30);
            tree.Root.Left.Key.Should().Be(10);
            tree.Root.Left.Left.Should().BeNull();
            tree.Root.Left.Right.Key.Should().Be(15);
            tree.Root.Left.Right.Left.Should().BeNull();
            tree.Root.Left.Right.Right.Key.Should().Be(20);
            tree.Root.Left.Right.Right.Left.Should().BeNull();
            tree.Root.Left.Right.Right.Right.Should().BeNull();

            tree.Root.Right.Key.Should().Be(40);
            tree.Root.Right.Left.Key.Should().Be(35);
            tree.Root.Right.Left.Left.Key.Should().Be(34);
            tree.Root.Right.Left.Left.Left.Should().BeNull();
            tree.Root.Right.Left.Left.Right.Should().BeNull();
            tree.Root.Right.Left.Right.Should().BeNull();
            tree.Root.Right.Right.Key.Should().Be(41);
            tree.Root.Right.Right.Left.Should().BeNull();
            tree.Root.Right.Right.Right.Should().BeNull();
        }

        [Fact]
        public void BST_SearchTest_Positive()
        {
            // Arrange
            BST tree = new();

            //Act
            tree.Insert(30);
            tree.Insert(10);
            tree.Insert(15);
            tree.Insert(20);

            tree.Insert(40);
            tree.Insert(35);
            tree.Insert(34);
            tree.Insert(41);

            //Assert
            tree.Search(34).Should().BeTrue();
            tree.Search(41).Should().BeTrue();
            tree.Search(15).Should().BeTrue();
        }

        [Fact]
        public void BST_SearchTest_Negative()
        {
            // Arrange
            BST tree = new();

            //Act
            tree.Insert(30);
            tree.Insert(10);
            tree.Insert(15);
            tree.Insert(20);

            tree.Insert(40);
            tree.Insert(35);
            tree.Insert(34);
            tree.Insert(41);

            //Assert
            tree.Search(300).Should().BeFalse();
            tree.Search(0).Should().BeFalse();
            tree.Search(32).Should().BeFalse();
        }

        [Fact]
        public void BST_RemoveTest()
        {
            // Arrange
            BST tree = new();

            //Act
            tree.Insert(30);
            tree.Insert(50);
            tree.Insert(35);
            tree.Insert(34);
            tree.Insert(61);
            tree.Insert(55);
            tree.Insert(54);
            tree.Insert(52);
            tree.Insert(58);

            tree.Remove(50);

            //Assert
            // It must be:
            /* Before delete 50
                  _____30_____
                              \
                              50
                             /  \
                           35    61
                          /     /
                         34    55
                              /  \          
                             54    58          
                            /
                           52
                  
            After delete 50
                  _____30_____
                              \
                              52
                             /  \
                           35    61
                          /     /
                         34    55
                              /  \          
                            54    58   */
            var root = tree.Root;
            root.Key.Should().Be(30);
            root.Left.Should().BeNull();
            root.Right.Key.Should().Be(52);
            root.Right.Left.Key.Should().Be(35);
            root.Right.Left.Left.Key.Should().Be(34);
            root.Right.Left.Left.Left.Should().BeNull();
            root.Right.Left.Left.Right.Should().BeNull();
            root.Right.Left.Right.Should().BeNull();
            root.Right.Right.Key.Should().Be(61);
            root.Right.Right.Left.Key.Should().Be(55);
            root.Right.Right.Left.Left.Key.Should().Be(54);
            root.Right.Right.Left.Left.Left.Should().BeNull();
            root.Right.Right.Left.Left.Right.Should().BeNull();
            root.Right.Right.Left.Right.Key.Should().Be(58);
            root.Right.Right.Left.Right.Left.Should().BeNull();
            root.Right.Right.Left.Right.Right.Should().BeNull();
            root.Right.Right.Right.Should().BeNull();
        }

        [Fact]
        public void BST_SortTest()
        {
            // Arrange
            BST tree = new();

            //Act
            tree.Insert(30);
            tree.Insert(50);
            tree.Insert(35);
            tree.Insert(34);
            tree.Insert(61);
            tree.Insert(55);
            tree.Insert(54);
            tree.Insert(52);
            tree.Insert(58);

            //Assert
            List<int> resultList = new();
            tree.Sort(i => resultList.Add(i));
            resultList.Aggregate(string.Empty, (t, n) => string.IsNullOrEmpty(t) ? n.ToString() : t + $" {n}")
                .Should().Be("30 34 35 50 52 54 55 58 61");
        }
    }
}
