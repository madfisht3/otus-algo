﻿using FluentAssertions;
using hw13;
using Xunit;

namespace Tests
{
    public class TrieTest
    {
        [Fact]
        public void InsertTest()
        {
            //Arrange
            var trie = new Trie();

            //Act
            trie.Insert("test");
            trie.Insert("testing");
            trie.Insert("test1");

            //Assert
            trie.Root.Children['t'].Should().NotBeNull();
            trie.Root.Children['t'].Children['e'].Should().NotBeNull();
            trie.Root.Children['t'].Children['e'].Children['s'].Should().NotBeNull();
            trie.Root.Children['t'].Children['e'].Children['s'].Children['t'].Should().NotBeNull();
            trie.Root.Children['t'].Children['e'].Children['s'].Children['t'].IsFinal.Should().BeTrue();

            trie.Root.Children['t'].Children['e'].Children['s'].Children['t'].Children['1'].Should().NotBeNull();
            trie.Root.Children['t'].Children['e'].Children['s'].Children['t'].Children['1'].IsFinal.Should().BeTrue();
        }

        [Fact]
        public void SearchTest_Positive()
        {
            //Arrange
            var trie = new Trie();
            trie.Insert("test");
            trie.Insert("testing");
            trie.Insert("test1");

            //Act
            bool result = trie.Search("test1");

            //Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void SearchTest_Negative()
        {
            //Arrange
            var trie = new Trie();
            trie.Insert("test");
            trie.Insert("testing");
            trie.Insert("test1");

            //Act
            bool result = trie.Search("tes");

            //Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void StartsWithTest()
        {
            //Arrange
            var trie = new Trie();
            trie.Insert("test");
            trie.Insert("testing");
            trie.Insert("test1");

            //Act
            bool result = trie.StartsWith("testi");

            //Assert
            result.Should().BeTrue();
        }
    }
}
