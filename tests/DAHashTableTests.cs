﻿using hw12;

namespace Tests
{
    public class DAHashTableTests : HashTableTests<DAHashTable<int, int>>
    {
        protected override IHashTable<int, int> MakeAnInstance() => new DAHashTable<int, int>();
    }
}
