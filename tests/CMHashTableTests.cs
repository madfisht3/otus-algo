﻿using hw12;

namespace Tests
{
    public class CMHashTableTests : HashTableTests<CMHashTable<int, int>>
    {
        protected override IHashTable<int, int> MakeAnInstance() => new CMHashTable<int,int>();
    }
}
