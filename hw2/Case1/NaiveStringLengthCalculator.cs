﻿namespace hw2
{
    public class NaiveStringLengthCalculator 
        : IStringLengthCalculator
    {
        public long CalculateLength(string inputString)
            => string.IsNullOrEmpty(inputString) ? 0 : inputString.Length;
    }
}
