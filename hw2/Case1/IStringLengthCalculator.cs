﻿namespace hw2
{
    public interface IStringLengthCalculator
    {
        long CalculateLength(string inputString);
    }
}
