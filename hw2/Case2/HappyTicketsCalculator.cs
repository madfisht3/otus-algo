﻿namespace hw2;

public class HappyTicketsCalculator : IHappyTicketsCalculator
{
    long _count = 0;

    public long Calculate(int n)
    {
        _count = 0;
        GetLuckyCount(n, 0, 0);
        return _count;
    }

    private void GetLuckyCount(int n, int sumA, int sumB)
    {
        if (n == 0)
        {
            if (sumA == sumB) _count++;

            return;
        }

        for (int a = 0; a <= 9; a++)
        {
            for (int b = 0; b <= 9; b++)
            {
                GetLuckyCount(n - 1, sumA + a, sumB + b);
            }
        }
    }
}
