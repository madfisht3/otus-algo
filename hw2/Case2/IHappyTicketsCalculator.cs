﻿namespace hw2;

public interface IHappyTicketsCalculator
{
    long Calculate(int n);
}
