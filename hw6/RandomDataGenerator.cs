﻿namespace SimpleSorting
{
    internal static class RandomDataGenerator
    {
        internal static double[] GenerateRandomDoubleArray(int size, int numberAfterDot, int minValue = -100, int maxValue = 100)
        {
            double[] result = new double[size];
            Random random = new();

            int numbersAfterDotMuiltiplexor = (int)Math.Pow(10, numberAfterDot);

            for (int i = 0; i < result.Length; i++)
            {
                double temp = random.Next(minValue * numbersAfterDotMuiltiplexor, maxValue * numbersAfterDotMuiltiplexor) / (double)numbersAfterDotMuiltiplexor;
                result[i] = temp;
            }

            return result;
        }

        internal static int[] GenerateRandomArrayInt(int size, int minValue = -100, int maxValue = 100)
        {
            int[] result = new int[size];
            Random random = new();
            for (int i = 0; i < result.Length; i++)
            {
                int temp = random.Next(minValue, maxValue);
                result[i] = temp;
            }

            return result;
        }
    }
}
