﻿using SimpleSorting;
using System.Diagnostics;
using hw6;

TestSorting(100);
TestSorting(1000);
TestSorting(10000);
Console.ReadLine();

void TestSorting(int arraySize)
{
    Console.WriteLine($"Array size = {arraySize}.");
    var testArray1 = RandomDataGenerator.GenerateRandomArrayInt(arraySize, -10000, 10000);

    var sw = Stopwatch.StartNew();
    testArray1.BubbleSort();
    sw.Stop();

    Console.WriteLine($"Bubble:\t\t{sw.ElapsedTicks} ticks");

    var testArray2 = RandomDataGenerator.GenerateRandomArrayInt(arraySize, -10000, 10000);

    sw = Stopwatch.StartNew();
    testArray2.InsertionSort();
    sw.Stop();
    Console.WriteLine($"Insertion:\t{sw.ElapsedTicks} ticks");

    var testArray3 = RandomDataGenerator.GenerateRandomArrayInt(arraySize, -10000, 10000);

    sw = Stopwatch.StartNew();
    testArray3.ShellSort();
    sw.Stop();

    Console.WriteLine($"Shell:\t\t{sw.ElapsedTicks} ticks");

    Console.WriteLine("---------------------------------");
}