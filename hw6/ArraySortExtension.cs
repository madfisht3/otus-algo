﻿namespace hw6;

public static class ArraySortExtension
{
    public static void BubbleSort<T>(this IList<T> collection) 
        where T : IComparable<T>
    {
        T temp;

        for (int i = 0; i < collection.Count; i++)
        {
            for (int j = 0; j < collection.Count - 1; j++)
            {
                if (collection[j].CompareTo(collection[j + 1]) == 1)
                {
                    temp = collection.ElementAt(j);
                    collection[j] = collection[j + 1];
                    collection[j + 1] = temp;
                }
            }
        }
    }

    public static void InsertionSort<T>(this IList<T> collection)
        where T : IComparable<T>
    {
        for (int i = 1; i < collection.Count; i++)
        {
            T key = collection[i];
            int j = i - 1;

            while (j >= 0 && collection[j].CompareTo(key) == 1)
            {
                collection[j + 1] = collection[j];
                j--;
            }
            collection[j + 1] = key;
        }
    }

    public static void ShellSort<T>(this IList<T> collection)
        where T : IComparable<T>
    {
        for (int gap = collection.Count / 2; gap > 0; gap /= 2)
        {
            for (int i = gap; i < collection.Count; i += 1)
            {
                T temp = collection[i];

                int j;
                for (j = i; j >= gap && collection[j - gap].CompareTo(temp) == 1; j -= gap)
                    collection[j] = collection[j - gap];

                collection[j] = temp;
            }
        }
    }
}
