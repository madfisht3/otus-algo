﻿namespace hw9
{
    public static class RadixSortExtension
    {
        public static void RadixSort(this int[] array)
        {
            if (array.Length <= 0) return;

            int[] result = new int[array.Length];

            int r = 2;
            int b = 32;

            int[] counting = new int[1 << r];
            int[] prefix = new int[1 << r];

            int groups = (int)Math.Ceiling((double)b / (double)r);
            int mask = (1 << r) - 1;

            for (int c = 0, shift = 0; c < groups; c++, shift += r)
            {
                for (int j = 0; j < counting.Length; j++)
                    counting[j] = 0;

                for (int i = 0; i < array.Length; i++)
                    counting[(array[i] >> shift) & mask]++;

                prefix[0] = 0;
                for (int i = 1; i < counting.Length; i++)
                    prefix[i] = prefix[i - 1] + counting[i - 1];

                for (int i = 0; i < array.Length; i++)
                    result[prefix[(array[i] >> shift) & mask]++] = array[i];

                for (int i = 0; i < result.Length; i++)
                {
                    array[i] = result[i];
                }
            }
        }
    }
}
