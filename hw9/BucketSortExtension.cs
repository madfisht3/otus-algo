﻿namespace hw9
{
    public static class BucketSortExtension
    {
        public static void BucketSort(this int[] array, int max)
        {
            if (array.Length <= 0) return;

            List<int>[] buckets = new List<int>[array.Length];

            for (int i = 0; i < array.Length; i++)
                buckets[i] = new List<int>();

            for (int i = 0; i < array.Length; i++)
            {
                long index = ((long)array[i] * array.Length) / (max + 1);

                buckets[index].Add(array[i]);
                if (buckets[index].Count > 1) buckets[index].Sort();
            }

            int counter = 0;
            foreach (var bucket in buckets)
                foreach (var item in bucket)
                    array[counter++] = item;
        }
    }
}
