﻿using hw9;
using System.Diagnostics;

for (int i = 100; i <= 1000000; i*= 10)
{
    Console.WriteLine($"Array length: {i}");
    TestSorting(i);
}

void TestSorting(int arrayLength)
{
    int max = 999;
    var randomArray1 = RandomDataGenerator.GenerateArray(arrayLength, 0, max);
    var randomArray2 = randomArray1.ToArray();
    var randomArray3 = randomArray1.ToArray();
    var randomArray4 = randomArray1.ToArray();

    Stopwatch sw = Stopwatch.StartNew();
    randomArray1.CountingSort(max);
    sw.Stop();
    Console.WriteLine($"CountingSort:\t{sw.ElapsedTicks} ticks\t{sw.ElapsedMilliseconds / 1000}.{sw.ElapsedMilliseconds} s");

    sw.Restart();
    randomArray2.BucketSort(max);
    sw.Stop();
    Console.WriteLine($"BucketSort:\t{sw.ElapsedTicks} ticks\t{sw.ElapsedMilliseconds / 1000}.{sw.ElapsedMilliseconds} s");

    sw.Restart();
    randomArray3.RadixSort();
    sw.Stop();
    Console.WriteLine($"RadixSort:\t{sw.ElapsedTicks} ticks\t{sw.ElapsedMilliseconds / 1000}.{sw.ElapsedMilliseconds} s");

    Array.Sort(randomArray4);
    Console.WriteLine("------------------------------------------");
    if (!randomArray1.SequenceEqual(randomArray4)) throw new Exception("randomArray1 isnt sorted correctly.");
    if (!randomArray2.SequenceEqual(randomArray4)) throw new Exception("randomArray2 isnt sorted correctly.");
    if (!randomArray3.SequenceEqual(randomArray4)) throw new Exception("randomArray3 isnt sorted correctly.");
}