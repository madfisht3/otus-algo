﻿namespace hw9
{
    public static class RandomDataGenerator
    {
        public static int[] GenerateArray(int length, int min, int max)
        {
            Random random = new();
            var result = new int[length];

            for (int i = 0; i < result.Length; i++)
            {
                result[i] = random.Next(min, max);
            }

            return result;
        }
    }
}
