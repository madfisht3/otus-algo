﻿namespace hw9
{
    public static class CountingSortExtension
    {
        public static void CountingSort(this int[] array, int max)
        {
            if (array.Length <= 0) return;

            int[] counting = new int[max + 1];
            int[] result = new int[array.Length];

            for (int i = 0; i < array.Length; i++)
                counting[array[i]]++;

            for (int i = 1; i < counting.Length; i++)
                counting[i] += counting[i - 1];

            for (int i = array.Length - 1; i >= 0; i--)
            {
                result[counting[array[i]] - 1] = array[i];
                counting[array[i]]--;
            }

            for (int i = 0; i < array.Length; i++)
                array[i] = result[i];
        }
    }
}
