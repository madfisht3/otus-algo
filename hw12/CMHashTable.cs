﻿namespace hw12
{
    /// <summary>
    /// The hash table with chain addressing. 
    /// </summary>
    /// <typeparam name="T">Key type</typeparam>
    /// <typeparam name="Y">Value type</typeparam>
    public class CMHashTable<T, Y> 
        : IHashTable<T, Y> 
        where T : struct where Y : struct
    {
        private readonly float _loadFactor = 0.75f;
        private readonly int _capacity = 11;
        private HashEntry<T, Y>[] _buckets;
        private int _size;
        private int _threshold;

        public CMHashTable()
        {
            _buckets = new HashEntry<T, Y>[_capacity];
            _threshold = (int)(_capacity * _loadFactor);
        }

        public void Add(T key, Y value)
        {
            int index = Hash(key);
            var entry = _buckets[index];

            while (entry != null)
            {
                if (entry.Key.Equals(key))
                {
                    entry.Value = value;
                    return;
                }
                else
                {
                    entry = entry.Next;
                }
            }

            if (++_size > _threshold)
            {
                Rehash();
                index = Hash(key);
            }

            entry = new HashEntry<T, Y>(key, value);
            entry.Next = _buckets[index];
            _buckets[index] = entry;
        }

        public bool ContainsKey(T key)
        {
            int index = Hash(key);
            var entry = _buckets[index];
            while (entry != null)
            {
                if (entry.Key.Equals(key))
                {
                    return true;
                }
                else
                {
                    entry = entry.Next;
                }
            }

            return false;
        }

        public bool ContainsValue(Y value)
        {
            foreach (var entry in _buckets)
            {
                var e = entry;
                while (e != null)
                {
                    if (e.Value.Equals(value))
                    {
                        return true;
                    }
                    else
                    {
                        e = e.Next;
                    }
                }
            }

            return false;
        }

        public void Remove(T key)
        {
            int index = Hash(key);
            var entry = _buckets[index];
            HashEntry<T, Y> last = null;

            while (entry != null)
            {
                if (entry.Key.Equals(key))
                {
                    if (last == null)
                        _buckets[index] = entry.Next;
                    else
                        last.Next = entry.Next;

                    _size--;
                    return;
                }
                last = entry.Next;
                entry = entry.Next;
            }

            throw new KeyNotFoundException($"{key}");
        }

        private Y GetValue(T key)
        {
            int index = Hash(key);
            var entry = _buckets[index];
            while (entry != null)
            {
                if (entry.Key.Equals(key))
                {
                    return entry.Value;
                }
                else
                {
                    entry = entry.Next;
                }
            }

            throw new KeyNotFoundException($"{key}");
        }

        private void SetValue(T key, Y value)
        {
            int index = Hash(key);

            var entry = _buckets[index];
            while (entry != null)
            {
                if (entry.Key.Equals(key))
                {
                    entry.Value = value;
                    return;
                }
                else
                {
                    entry = entry.Next;
                }
            }

            throw new KeyNotFoundException($"{key}");
        }

        private int Hash(T key)
        {
            int hash = key.GetHashCode() % _buckets.Length;
            return hash < 0 ? -hash : hash;
        }

        private void Rehash()
        {
            var oldBuckets = _buckets;

            int newCapacity = (_buckets.Length * 2) + 1;
            _threshold = (int)(newCapacity * _loadFactor);
            _buckets = new HashEntry<T, Y>[newCapacity];

            HashEntry<T, Y> next;

            for (int i = 0; i < oldBuckets.Length; i++)
            {
                var entry = oldBuckets[i];
                while (entry != null)
                {
                    int index = Hash(entry.Key);
                    var dest = _buckets[index];

                    if (dest != null)
                    {
                        next = dest.Next;
                        while (next != null)
                        {
                            dest = next;
                            next = dest.Next;
                        }
                        dest.Next = entry;
                    }
                    else
                    {
                        _buckets[index] = entry;
                    }

                    next = entry.Next;
                    entry.Next = null;
                    entry = next;
                }
            }
        }

        public Y this[T key]
        {
            get => GetValue(key);
            set => SetValue(key, value);
        }

        public int Size => _size;
    }
}
