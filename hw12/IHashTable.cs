﻿namespace hw12
{
    public interface IHashTable<T, Y> where T : struct where Y : struct
    {
        void Add(T key, Y value);
        void Remove(T key);

        bool ContainsKey(T key);
        bool ContainsValue(Y value);

        Y this[T key] { get; set; }
        int Size { get; }
    }
}