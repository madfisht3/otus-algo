﻿namespace hw12
{
    public class DAHashEntry<T, Y> where T : struct where Y : struct
    {
        public DAHashEntry()
        {

        }

        public DAHashEntry(T key, Y value)
        {
            Key = key;
            Value = value;
        }

        public bool IsDeleted { get; set; }
        public T Key { get; set; }
        public Y Value { get; set; }
    }
}
