﻿namespace hw12
{
    public class HashEntry<T, Y>
        where T : struct
        where Y : struct
    {
        public HashEntry()
        {

        }

        public HashEntry(T key, Y value)
        {
            Key = key;
            Value = value;
        }

        public HashEntry<T,Y> Next { get; set; }

        public T Key { get; }
        public Y Value { get; set; }
    }
}