﻿namespace hw12
{
    /// <summary>
    /// The hash table with direct addressing.
    /// </summary>
    /// <typeparam name="T">Key type</typeparam>
    /// <typeparam name="Y">Value type</typeparam>
    public class DAHashTable<T, Y>
        : IHashTable<T, Y>
        where T : struct where Y : struct
    {
        private readonly float _loadFactor = 0.75f;
        private int _capacity = 11;
        private DAHashEntry<T, Y>?[] _bucket;
        private int _size;
        private int _threshold;

        public DAHashTable()
        {
            _bucket = new DAHashEntry<T, Y>?[_capacity];
            _threshold = (int)(_capacity * _loadFactor);
        }

        public void Add(T key, Y value)
        {
            int index = Hash(key);

            var entry = _bucket[index];

            while (index < _bucket.Length && entry != null && entry.IsDeleted)
                entry = _bucket[++index];

            if (entry != null)
            {
                Rehash();
                Add(key, value);
                return;
            }

            _bucket[index] = new DAHashEntry<T, Y>(key, value);
            _size++;
        }

        public bool ContainsKey(T key)
        {
            int index = Hash(key);
            while (index < _bucket.Length && _bucket[index] != null)
            {
                if (_bucket[index].Key.Equals(key) && !_bucket[index].IsDeleted) return true;

                index++;
            }

            return false;
        }

        public bool ContainsValue(Y value)
        {
            foreach (var keyValuePair in _bucket)
            {
                if (keyValuePair != null && keyValuePair.Value.Equals(value) && !keyValuePair.IsDeleted) return true;
            }

            return false;
        }

        public void Remove(T key)
        {
            int index = Hash(key);

            var entry = _bucket[index];

            while (index < _bucket.Length && entry != null)
            {
                entry = _bucket[index++];
                if (entry != null && !entry.IsDeleted)
                {
                    entry.IsDeleted = true;
                    return;
                }
            }
        }

        private int Hash(T key)
        {
            int hash = key.GetHashCode() % _bucket.Length;
            return hash < 0 ? -hash : hash;
        }

        private void Rehash()
        {
            int newCapacity = (_bucket.Length * 2) + 1;
            var oldBucket = _bucket;
            _bucket = new DAHashEntry<T, Y>[newCapacity];

            foreach (var keyValuePair in oldBucket)
            {
                if (keyValuePair == null || keyValuePair.IsDeleted) continue;

                int index = Hash(keyValuePair.Key);
                _bucket[index] = keyValuePair;
            }

            _capacity = newCapacity;
        }

        private Y GetValue(T key)
        {
            int index = Hash(key);

            var entry = _bucket[index];

            while (index < _bucket.Length && entry != null)
            {
                if (entry.Key.Equals(key) && !entry.IsDeleted) return entry.Value;
                entry = _bucket[++index];
            }

            throw new KeyNotFoundException(key.ToString());
        }

        private void SetValue(T key, Y value)
        {
            int index = Hash(key);

            var entry = _bucket[index];

            while (index < _bucket.Length && entry != null)
            {
                if (entry.Key.Equals(key))
                {
                    entry.Value = value;
                    return;
                }

                entry = _bucket[++index];
            }

            Add(key, value);
        }

        public Y this[T key]
        {
            get => GetValue(key);
            set => SetValue(key, value);
        }
        public int Size => _size;
    }
}
