﻿using hw4;

string separator = "--------------------------------";

// #1 Решить задачу про короля.
int position = 0;
Console.WriteLine($"King position {position}");
(int, ulong) kingResult = new KingMoveCalulator().Calculate(1UL << 0);
Console.WriteLine($"King moves: {kingResult.Item2}. Move count: {kingResult.Item1}.");
Console.WriteLine(separator);

// #2 Решить задачу про коня.
position = 0;
Console.WriteLine($"Knight position {position}");
(int, ulong) knightResult = new KnightMoveCalulator().Calculate(1UL << 0);
Console.WriteLine($"Knight moves: {knightResult.Item2}. Move count: {knightResult.Item1}.");
Console.WriteLine(separator);

// #3 Реализовать два алгоритма подсчёта единичных битов в числе.
uint mask = 2290649224; // 8 bit

Console.WriteLine($"Number: {ToConvinientBinaryString(mask)}");

Console.WriteLine($"First algorithm: {(BitCounter.Algorithm1(mask))}");
Console.WriteLine($"Second algorithm: {(BitCounter.Algorithm2(mask))}");

Console.ReadLine();

string ToConvinientBinaryString(uint mask)
{
    return $"{Convert.ToString(mask, 2)}b";
}
