﻿namespace hw4;

public class KnightMoveCalulator
{
    ulong _nA = 0xFeFeFeFeFeFeFeFe;
    ulong _nAB = 0xFcFcFcFcFcFcFcFc;
    ulong _nH = 0x7f7f7f7f7f7f7f7f;
    ulong _nGH = 0x3f3f3f3f3f3f3f3f;
    public (int, ulong) Calculate(ulong position)
    {
        ulong resultMask = _nGH & (position << 6 | position >> 10)
                         | _nH & (position << 15 | position >> 17)
                         | _nA & (position << 17 | position >> 15)
                         | _nAB & (position << 10 | position >> 6);

        return (BitCounter.Algorithm1(resultMask), resultMask);
    }
}
