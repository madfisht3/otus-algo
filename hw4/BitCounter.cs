﻿namespace hw4
{
    public static class BitCounter
    {
        public static int Algorithm1(ulong mask)
        {
            ulong temp = mask;
            int result = 0;

            while (temp > 0)
            {
                if ((temp & 1) == 1) result++;
                temp >>= 1;
            }

            return result;
        }

        public static int Algorithm2(ulong mask)
        {
            ulong temp = mask;
            int result = 0;

            while (temp > 0)
            {
                result++;
                temp &= temp - 1;
            }

            return result;
        }
    }
}
