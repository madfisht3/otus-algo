﻿using hw4;

public class KingMoveCalulator
{
    ulong _rightBorder = 0xFeFeFeFeFeFeFeFe;
    ulong _leftBorder = 0x7f7f7f7f7f7f7f7f;

    public (int, ulong) Calculate(ulong position)
    {
        ulong resultMask = 0;
        resultMask |= (position << 1) & _rightBorder; // Влево
        resultMask |= (position >> 8); // Вниз
        resultMask |= (position >> 1) & _leftBorder; // Вправо
        resultMask |= (position << 8); // Вверх

        resultMask |= (position >> 7) & _rightBorder; // Влево вниз
        resultMask |= (position << 7) & _leftBorder; // Вправо вверх
        resultMask |= (position << 9) & _rightBorder; // Влево вверх
        resultMask |= (position >> 9) & _leftBorder; // Вправо вниз

        return (BitCounter.Algorithm1(resultMask), resultMask);
    }
}
