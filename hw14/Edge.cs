﻿namespace hw14
{
    public struct Edge
    {
        public Edge(string name, int verticeA, int verticeB)
        {
            Name = name;
            VerticeA = verticeA;
            VerticeB = verticeB;
        }

        public string Name { get; }
        public int VerticeA { get; }
        public int VerticeB { get; }

        public override string ToString() => $"Name: {Name}. Vertice A: {VerticeA}. Vertice B: {VerticeB}.";
    }
}