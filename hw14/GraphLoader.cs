﻿namespace hw14
{

    public static class GraphLoader
    {
        public static Graph LoadFromFile(string filename)
        {
            StreamReader reader = null;
            try
            {
                reader = new StreamReader(filename);

                string[] parts;
                parts = reader.ReadLine().Split(',');
                int verticesCount = int.Parse(parts[0]);
                var result = new Graph(verticesCount);
                int edgesCount = int.Parse(parts[1]);

                char edgeName = 'a';
                for (int i = 0; i < edgesCount; i++, edgeName++)
                {
                    parts = reader.ReadLine().Split(',');
                    int verticeA = int.Parse(parts[0]);
                    int verticeB = int.Parse(parts[1]);
                    result.AddEdge(edgeName.ToString(), verticeA, verticeB);
                }

                return result;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Couldn't load the graph: {ex}");
                throw;
            }
            finally
            {
                reader?.Dispose();
            }
        }
    }
}