﻿using hw14;

var graph = GraphLoader.LoadFromFile("grapth1.graph");

foreach (var edge in graph.Edges)
{
    Console.WriteLine(edge);
}

Console.WriteLine();

var vertices = graph.Vertices;
for (int i = 0; i < vertices.GetLength(0); i++)
{
    for (int j = 0; j < vertices.GetLength(1); j++)
    {
        var edge = vertices[i,j];
        if (string.IsNullOrEmpty(edge)) continue;

        Console.WriteLine($"Vertice: {i + 1} has edge: {edge} with vertice: {j + 1}");
    }
}

Console.WriteLine();

Console.WriteLine($"Is vertice {1} and vertice {2} are adjacency: {graph.VerticeAdjacency(1, 2)}");
Console.WriteLine($"Is vertice {3} and vertice {5} are adjacency: {graph.VerticeAdjacency(3, 5)}");
Console.WriteLine($"Is vertice {5} and vertice {5} are adjacency: {graph.VerticeAdjacency(5, 5)}");
Console.WriteLine($"Is vertice {3} and vertice {3} are adjacency: {graph.VerticeAdjacency(3, 3)}");

Console.WriteLine();

foreach (var vertice in graph.AdjacencyVertices(2))
    Console.WriteLine($"Vertice 2 and vertice {vertice} are adjacency.");

foreach (var vertice in graph.AdjacencyVertices(5))
    Console.WriteLine($"Vertice 5 and vertice {vertice} are adjacency.");

Console.WriteLine();

Console.WriteLine($"Power of vertice 1: {graph.VerticePower(1)}");
Console.WriteLine($"Power of vertice 2: {graph.VerticePower(2)}");
Console.WriteLine($"Power of vertice 3: {graph.VerticePower(3)}");
Console.WriteLine($"Power of vertice 4: {graph.VerticePower(4)}");
Console.WriteLine($"Power of vertice 5: {graph.VerticePower(5)}");

Console.ReadLine();
