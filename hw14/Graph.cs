﻿namespace hw14
{
    public class Graph
    {
        private string[,] _vertices;
        private List<Edge> _edges = new List<Edge>();

        public Graph(int verticesCount)
        {
            _vertices = new string[verticesCount, verticesCount];
        }

        public void AddEdge(string name, int verticeA, int verticeB)
        {
            _edges.Add(new Edge(name, verticeA, verticeB));
            _vertices[verticeA - 1, verticeB - 1] = name;
            _vertices[verticeB - 1, verticeA - 1] = name;
        }

        public bool VerticeAdjacency(int verticeA, int verticeB)
        {
            return _edges.Any(e =>
            {
                return e.VerticeA == verticeA && e.VerticeB == verticeB
                || e.VerticeA == verticeB && e.VerticeB == verticeA;
            });
        }

        public List<int> AdjacencyVertices(int vertice)
        {
            List<int> result = new List<int>();

            for (int i = 0; i < _vertices.GetLength(0); i++)
                if (!string.IsNullOrEmpty(_vertices[vertice - 1, i])) result.Add(i + 1);

            return result;
        }
        
        public int VerticePower(int vertice)
        {
            return _edges.Count(e => e.VerticeA == vertice || e.VerticeB == vertice);
        }

        public string[,] Vertices => _vertices;
        public IList<Edge> Edges => _edges;
    }
}