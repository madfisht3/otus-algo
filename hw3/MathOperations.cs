﻿namespace hw3
{
    public static class MathOperations
    {
        public static ulong IterativePower(uint number, uint power)
        {
            ulong result = 1;
            ulong baseNumber = number;

            for (uint i = power; i > 1; i /= 2)
            {
                if (i % 2 == 1) result *= baseNumber;

                baseNumber *= baseNumber;
            }

            if (power > 0) result *= baseNumber;

            return result;
        }

        public static ulong FibonacciRecursive(ulong number)
        {
            if (number == 0) return 0;
            if (number < 2) return 1;

            return FibonacciRecursive(number - 1) + FibonacciRecursive(number - 2);
        }

        public static ulong[] FibonacciIterative(ulong number)
        {
            ulong[] result = new ulong[number + 2];

            result[0] = 0;
            result[1] = 1;

            for (ulong i = 2; i <= number; i++)
            {
                result[i] = result[i - 1] + result[i - 2];
            }

            return result;
        }

        internal static uint PrimeNumbersCount(uint n)
        {
            uint count = 0;

            for (uint i = 2; i <= n; i++)
                if (IsPrime(i)) count++;

            return count;
        }

        private static bool IsPrime(uint number)
        {
            if (number == 2) return true;
            if (number % 2 == 0) return false;

            for (uint i = 3; i < number; i++)
                if (number % i == 0) return false;

            return true;
        }
    }

}
