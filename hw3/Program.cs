﻿using hw3;

string separator = "\r\n----------------------------------------------------\r\n";
Console.WriteLine($"#1\r\n");
// 1. + 1 байт.Реализовать итеративный O(N) алгоритм возведения числа в степень.


uint number = 5;
for (uint i = 0; i <= 20; i++)
{
    var actual = MathOperations.IterativePower(number, i);
    var expected = Math.Pow(number, i);
    if (actual != expected) throw new InvalidOperationException($"Результаты не совпадают. Степень: {i}");

    Console.WriteLine($"{number} ^ {i} = {actual}");
}
// 2. +1 байт. Реализовать рекурсивный O(2^N) и итеративный O(N) алгоритмы поиска чисел Фибоначчи.

Console.WriteLine($"{separator}#2\r\n");

var numbers = MathOperations.FibonacciIterative(25);

for (ulong i = 0; i < 25; i++)
{
    var recursive = MathOperations.FibonacciRecursive(i);
    var iterative = numbers[i];

    if (recursive != iterative) throw new InvalidOperationException($"Результаты не совпадают. Индекс: {i}");

    Console.WriteLine($"{i} = {recursive}; {iterative}");
}

// 3. +1 байт. Реализовать алгоритм поиска количества простых чисел через перебор делителей, O(N ^ 2).

Console.WriteLine($"{separator}#3\r\n");

for (uint i = 10; i < 1000000; i *= 10)
{
    var count = MathOperations.PrimeNumbersCount(i);
    Console.WriteLine($"{i} -> {count}");
}

Console.ReadLine();