﻿using hw10;
using System.Diagnostics;

BST tree1 = new();
BST tree2 = new();
string separator = "---------------------------";
HashSet<int> randomSet = new();
HashSet<int> sequentialSet = new();


Console.WriteLine($"Fill random:");
FillRandomly(tree1, randomSet);
Console.WriteLine();

Console.WriteLine($"Fill sequentially:");
FillSequentially(tree2, sequentialSet);
Console.WriteLine(separator);


Console.WriteLine($"Find in random tree:");
FindRandomHundred(tree1, randomSet);
Console.WriteLine();

Console.WriteLine($"Find in sequential tree:");
FindRandomHundred(tree2, sequentialSet);
Console.WriteLine(separator);


Console.WriteLine($"Delete in random tree:");
DeleteRandomHundred(tree1, randomSet);
Console.WriteLine();

Console.WriteLine($"Delete in sequential tree:");
DeleteRandomHundred(tree2, sequentialSet);


void FillRandomly(BST tree, HashSet<int> set)
{
    Random random = new();
    while (set.Count <= 100)
    {
        set.Add(random.Next(0, 1000));
    }

    Stopwatch sw = Stopwatch.StartNew();
    foreach (var item in set)
    {
        tree.Insert(item);
    }
    sw.Stop();
    Console.WriteLine($"{sw.ElapsedTicks} ticks");
}

void FillSequentially(BST tree, HashSet<int> set)
{
    Enumerable.Range(0, 101)
        .ToList().ForEach(i => set.Add(i));

    Stopwatch sw = Stopwatch.StartNew();
    foreach (var item in set)
    {
        tree.Insert(item);
    }
    sw.Stop();
    Console.WriteLine($"{sw.ElapsedTicks} ticks");
}

void FindRandomHundred(BST tree, HashSet<int> set)
{
    Stopwatch sw = Stopwatch.StartNew();
    foreach (var item in set)
    {
        tree.Search(item);
    }
    sw.Stop();
    Console.WriteLine($"{sw.ElapsedTicks} ticks");
}

void DeleteRandomHundred(BST tree, HashSet<int> set)
{
    Random random = new();

    Stopwatch sw = Stopwatch.StartNew();
    foreach (var item in set)
    {
        if (tree.Root.Key == item) continue;
        tree.Remove(item);
    }
    sw.Stop();
    Console.WriteLine($"{sw.ElapsedTicks} ticks");
}