﻿namespace hw10
{
    public class BST
    {
        public BSTItem Root { get; private set; }

        public void PrintTree(BSTItem item)
        {
            if (item == null) return;
            PrintTree(item.Left);

        }

        public void Insert(int key)
        {
            if (Root == null)
            {
                Root = new BSTItem(key, null);
            }
            else
            {
                BSTItem currentItem = Root;
                do
                {
                    if (currentItem.Key == key) return;

                    if (currentItem.Key > key) //Left
                    {
                        if (currentItem.Left == null) break;
                        currentItem = currentItem.Left;
                    }
                    else //Right
                    {
                        if (currentItem.Right == null) break;
                        currentItem = currentItem.Right;
                    }
                } while (true);

                var newItem = new BSTItem(key, currentItem);

                if (currentItem.Key > key)
                {
                    currentItem.Left = newItem;
                }
                else
                {
                    currentItem.Right = newItem;
                }
            }
        }

        public void Sort() => SortInternal(Root, Console.WriteLine);

        public void Sort(Action<int> action) => SortInternal(Root, action);

        private void SortInternal(BSTItem item, Action<int> action)
        {
            if (item == null) return;
            SortInternal(item.Left, action);

            action?.Invoke(item.Key);

            SortInternal(item.Right, action);
        }


        public bool Search(int key)
        {
            return SearchInternal(Root, key) != null;
        }

        private BSTItem SearchInternal(BSTItem item, int key)
        {
            if (item == null) return null;
            if (key == item.Key) return item;

            return key < item.Key
                ? SearchInternal(item.Left, key)
                : SearchInternal(item.Right, key);
        }


        public void Remove(int key)
        {
            var item = SearchInternal(Root, key);
            if (item == Root) throw new InvalidOperationException("You can't remove the root.");

            if (item == null) return;

            if (item.Left == null && item.Right == null) // it doesn't have children
            {
                RemoveEmpty(item);
            }
            else if ((item.Left != null && item.Right == null) || (item.Left == null && item.Right != null)) // it has the only one children
            {
                RemoveOnlyOne(item);
            }
            else if (item.Left != null && item.Right != null) // it has two children
            {
                RemoveTwoChildren(item);
            }
        }

        private void RemoveEmpty(BSTItem item)
        {
            var parent = item.Parent;
            if (parent.Left == item)
            {
                parent.Left = null;
            }
            else if (parent.Right == item)
            {
                parent.Right = null;
            }
        }

        private void RemoveOnlyOne(BSTItem item)
        {
            var parent = item.Parent;
            var itemToReplace = item.Right == null ? item.Left : item.Right;

            if (parent.Left == item)
            {
                parent.Left = itemToReplace;
            }
            else if (parent.Right == item)
            {
                parent.Right = itemToReplace;
            }
        }

        private void RemoveTwoChildren(BSTItem item)
        {
            var itemToReplace = item.Right;
            while (true)
            {
                if (itemToReplace.Left == null) break;
                itemToReplace = itemToReplace.Left;
            }

            var tempKey = item.Key;
            item.Key = itemToReplace.Key;
            itemToReplace.Key = tempKey;

            item.Parent.Left = null;
            itemToReplace.Parent.Left = null;
        }
    }
}
