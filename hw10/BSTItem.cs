﻿using System.Diagnostics;

namespace hw10
{
    [DebuggerDisplay("Key = {Key}")]
    public class BSTItem
    {
        public BSTItem(int key, BSTItem parent)
        {
            Key = key;
            Parent = parent;
        }

        public int Key { get; set; }
        public BSTItem Left { get; set; }
        public BSTItem Right { get; set; }
        public BSTItem Parent { get; set; }
    }
}
