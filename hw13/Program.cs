﻿using hw13;

var trie = new Trie();

trie.Insert("hello");
trie.Insert("hell");
trie.Insert("test");
trie.Insert("testing");
trie.Insert("test123");

Console.WriteLine(trie.Search("hello"));
Console.WriteLine(trie.Search("hell"));
Console.WriteLine(trie.Search("test"));
Console.WriteLine(trie.Search("testing"));
Console.WriteLine(trie.Search("test123"));

Console.WriteLine(trie.StartsWith("hell"));
Console.WriteLine(trie.StartsWith("hel"));
Console.WriteLine(trie.StartsWith("tes"));
Console.WriteLine(trie.StartsWith("testing"));
Console.WriteLine(trie.StartsWith("test1"));