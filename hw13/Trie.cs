﻿namespace hw13
{
    public class Trie
    {
        public Trie()
        {
            Root = new Node();
        }

        public virtual void Insert(string word)
        {
            Node node = Root;

            foreach (var symbol in word)
            {
                node = node.Add(symbol);
            }

            node.IsFinal = true;
        }

        public virtual bool Search(string word)
        {
            Node node = Root;

            foreach (var symbol in word)
            {
                if (node.Children[symbol] == null) return false;

                node = node.Children[symbol];
            }

            return node.IsFinal;
        }

        public virtual bool StartsWith(string prefix)
        {
            Node node = Root;

            foreach (var symbol in prefix)
            {
                if (node.Children[symbol] == null) return false;

                node = node.Children[symbol];
            }

            return true;
        }

        public Node Root { get; }

        public class Node
        {
            private readonly int _size = 128;

            public Node()
            {
                Children = new Node[_size];
            }

            public Node Add(char symbol)
            {
                if (Children[symbol] == null)
                {
                    Children[symbol] = new Node();
                }

                return Children[symbol];
            }

            public Node[] Children { get; }
            public bool IsFinal { get; set; }
        }
    }
}
