﻿namespace hw16
{
    public class WeightedGraph
    {
        private string[,] _vertices;
        private List<EdgeExtended> _edges = new();

        public WeightedGraph(int verticesCount)
        {
            _vertices = new string[verticesCount, verticesCount];
        }

        public void AddEdge(string name, int verticeA, int verticeB, int weight)
        {
            _edges.Add(new EdgeExtended(name, verticeA, verticeB, weight));
            _vertices[verticeA - 1, verticeB - 1] = name;
            _vertices[verticeB - 1, verticeA - 1] = name;
        }

        public bool VerticeAdjacency(int verticeA, int verticeB)
        {
            return _edges.Any(e =>
            {
                return e.VerticeA == verticeA && e.VerticeB == verticeB
                || e.VerticeA == verticeB && e.VerticeB == verticeA;
            });
        }

        public List<int> AdjacencyVertices(int vertice)
        {
            List<int> result = new();

            for (int i = 0; i < _vertices.GetLength(0); i++)
                if (!string.IsNullOrEmpty(_vertices[vertice - 1, i])) result.Add(i + 1);

            return result;
        }

        public int VerticePower(int vertice)
        {
            return _edges.Count(e => e.VerticeA == vertice || e.VerticeB == vertice);
        }

        public string[,] Vertices => _vertices;
        public IList<EdgeExtended> Edges => _edges;
    }
}
