﻿namespace hw16
{
    public struct EdgeExtended
    {
        public EdgeExtended(string name, int verticeA, int verticeB, int weight)
        {
            Name = name;
            VerticeA = verticeA;
            VerticeB = verticeB;
            Weight = weight;
        }

        public string Name { get; }
        public int VerticeA { get; }
        public int VerticeB { get; }
        public int Weight { get; }

        public override string ToString() => $"Name: {Name}. Vertice A: {VerticeA}. Vertice B: {VerticeB}. Weight: {Weight}.";
    }
}
