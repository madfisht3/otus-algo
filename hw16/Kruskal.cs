﻿using hw14;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hw16
{
    public static class Kruskal
    {
        public static EdgeExtended[] GetMinimumSpanningForest(this WeightedGraph graph)
        {
            int verticeCount = graph.Vertices.GetLength(0);
            var result = new EdgeExtended[verticeCount];
            int i = 0;
            int edgeNumber = 0;

            var sortedEdges = graph.Edges.OrderBy(e => e.Weight).ToArray();

            Subset[] subsets = new Subset[verticeCount];

            for (int v = 0; v < verticeCount; ++v)
            {
                subsets[v].Parent = v;
                subsets[v].Rank = 0;
            }

            while (edgeNumber < verticeCount - 1)
            {
                var next = graph.Edges[i++];
                int x = Find(subsets, next.VerticeA - 1);
                int y = Find(subsets, next.VerticeB - 1);

                if (x != y)
                {
                    result[edgeNumber++] = next;
                    Union(subsets, x, y);
                }
            }

            return result.Where(e => e.Weight > 0).ToArray();
        }

        private struct Subset
        {
            public int Parent;
            public int Rank;
        }

        private static int Find(Subset[] subsets, int i)
        {
            if (subsets[i].Parent != i)
                subsets[i].Parent = Find(subsets, subsets[i].Parent);

            return subsets[i].Parent;
        }

        private static void Union(Subset[] subsets, int x, int y)
        {
            int xroot = Find(subsets, x);
            int yroot = Find(subsets, y);

            if (subsets[xroot].Rank < subsets[yroot].Rank)
                subsets[xroot].Parent = yroot;
            else if (subsets[xroot].Rank > subsets[yroot].Rank)
                subsets[yroot].Parent = xroot;
            else
            {
                subsets[yroot].Parent = xroot;
                ++subsets[xroot].Rank;
            }
        }
    }
}
