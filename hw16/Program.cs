﻿using hw16;

WeightedGraph graph = GenerateGparh();

var minimumSF = graph.GetMinimumSpanningForest();

foreach (EdgeExtended edge in minimumSF)
    Console.WriteLine($"Edge: ({edge.Name})\t{edge.VerticeA} -> {edge.VerticeB} ");

Console.ReadLine();


static WeightedGraph GenerateGparh()
{
    var result = new WeightedGraph(5);

    result.AddEdge("1", 1, 2, 1);
    result.AddEdge("2", 4, 5, 2);
    result.AddEdge("3", 1, 3, 3);
    result.AddEdge("4", 2, 3, 4);
    result.AddEdge("5", 3, 4, 5);
    result.AddEdge("6", 2, 4, 6);
    result.AddEdge("7", 2, 5, 7);

    return result;
}