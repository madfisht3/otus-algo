﻿namespace hw5
{
    public class SingleArray<T>
        : IDynamicArray<T> where T : struct
    {
        private T[] _internalArray = Array.Empty<T>();

        public void Add(T item)
        {
            IncreaseSize();
            _internalArray[Size - 1] = item;
        }

        public void Add(T item, int index)
        {
            if (index >= Size)
            {
                Add(item);
            }
            else
            {
                var newArray = new T[_internalArray.Length + 1];

                for (int i = 0, j = 0; i < newArray.Length; i++)
                {
                    if (i == index)
                    {
                        newArray[i] = item;
                        continue;
                    }

                    newArray[i] = _internalArray[j++];
                }

                _internalArray = newArray;
            }
        }

        private void IncreaseSize()
        {
            var newArray = new T[_internalArray.Length + 1];
            Array.Copy(_internalArray, newArray, _internalArray.Length);
            _internalArray = newArray;
        }

        public void Remove(int index)
        {
            var newArray = new T[_internalArray.Length - 1];

            for (int i = 0, j = 0; i < _internalArray.Length; i++)
            {
                if (i == index) continue;

                newArray[j++] = _internalArray[i];
            }

            _internalArray = newArray;
        }

        public int Size => _internalArray.Length;

        public T this[int index]
        {
            get => _internalArray[index];
            set => _internalArray[index] = value;
        }
    }
}
