﻿namespace hw5
{
    public class FactorArray<T> : IDynamicArray<T> where T : struct
    {
        private T[] _internalArray;
        private readonly int _factor;
        private readonly int _initLength;
        private int _size;

        public FactorArray() : this(2, 10) { }
        public FactorArray(int factor, int initLength)
        {
            _factor = factor;
            _initLength = initLength;
            _size = 0;
            _internalArray = new T[initLength];
        }

        public void Add(T item)
        {
            if (_internalArray.Length == Size) IncreaseSize();

            _size++;
            _internalArray[Size - 1] = item;
        }

        public void Add(T item, int index)
        {

            if (_internalArray.Length == Size)
            {
                _size++;
                var newArray = new T[_internalArray.Length * _factor];
                for (int i = 0, j = 0; i < Size; i++)
                {
                    if (index == i)
                    {
                        newArray[i] = item;
                        continue;
                    }

                    newArray[i] = _internalArray[j++];
                }

                _internalArray = newArray;
            }
            else
            {
                T temp;
                _internalArray[_size] = default;
                for (int i = _size; i > index; i--)
                {
                    temp = _internalArray[i];
                    _internalArray[i] = _internalArray[i - 1];
                    _internalArray[i - 1] = temp;
                }

                _internalArray[index] = item;
                _size++;
            }
        }

        private void IncreaseSize()
        {
            var newArray = new T[_internalArray.Length * _factor];
            Array.Copy(_internalArray, newArray, _internalArray.Length);
            _internalArray = newArray;
        }

        public void Remove(int index)
        {

            if (_internalArray.Length == Size * _factor && _internalArray.Length > _initLength)
            {
                _size--;
                var newArray = new T[_internalArray.Length / _factor];

                for (int i = 0, j = 0; i < newArray.Length; i++)
                {
                    if (index == i) continue;
                    newArray[j++] = _internalArray[i];
                }

                _internalArray = newArray;
            }
            else
            {
                _size--;
                for (int i = index; i < _internalArray.Length - 1; i++)
                {
                    _internalArray[i] = _internalArray[i + 1];
                }

                _internalArray[_size] = default;
            }
        }

        public int Size => _size;

        public T this[int index]
        {
            get => _internalArray[index];
            set => _internalArray[index] = value;
        }
    }
}
