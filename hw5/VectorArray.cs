﻿namespace hw5
{
    public class VectorArray<T> : IDynamicArray<T> where T : struct
    {
        private T[] _internalArray = Array.Empty<T>();
        private readonly int _vector;
        private int _size;

        public VectorArray() : this(10) { }
        public VectorArray(int vector)
        {
            _vector = vector;
            _size = 0;
        }

        public void Add(T item)
        {
            if (_internalArray.Length == Size) IncreaseSize();

            _size++;
            _internalArray[Size - 1] = item;
        }

        public void Add(T item, int index)
        {
            if (index >= Size)
            {
                Add(item);
            }
            else
            {
                if (_internalArray.Length == Size)
                {
                    var newArray = new T[_internalArray.Length + _vector];

                    for (int i = 0, j = 0; i < newArray.Length; i++)
                    {
                        if (i == index)
                        {
                            newArray[i] = item;
                            continue;
                        }

                        newArray[i] = _internalArray[j++];
                    }

                    _internalArray = newArray;
                }
                else
                {
                    T temp;
                    _internalArray[_size] = default;
                    for (int i = _size; i > index; i--)
                    {
                        temp = _internalArray[i];
                        _internalArray[i] = _internalArray[i - 1];
                        _internalArray[i - 1] = temp;
                    }

                    _internalArray[index] = item;
                }

                _size++;
            }
        }

        private void IncreaseSize()
        {
            var newArray = new T[_internalArray.Length + _vector];
            Array.Copy(_internalArray, newArray, _internalArray.Length);
            _internalArray = newArray;
        }

        public void Remove(int index)
        {
            _size--;
            if (_size % _vector == 0)
            {
                var newArray = new T[_internalArray.Length - _vector];

                for (int i = 0, j = 0; i <= _size; i++)
                {
                    if (i == index) continue;

                    newArray[j++] = _internalArray[i];
                }

                _internalArray = newArray;
            }
            else
            {
                for (int i = index; i < _internalArray.Length - 1; i++)
                {
                    _internalArray[i] = _internalArray[i + 1];
                }

                _internalArray[_size] = default;
            }
        }

        public int Size => _size;

        public T this[int index]
        {
            get => _internalArray[index];
            set => _internalArray[index] = value;
        }
    }
}
