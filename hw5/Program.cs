﻿using hw5;
using System.Diagnostics;

Console.WriteLine("Таблица сравнения производительности");

int itemsCount = short.MaxValue;

var single = new SingleArray<int>();
var vector = new VectorArray<int>();
var factor = new FactorArray<int>();
long ticks;

Console.WriteLine("Вставка в конец");
Console.WriteLine("---------------------------------------");

Console.Write("Single: ");
ticks = InsertToEndTest(single, itemsCount);
Console.WriteLine($"{ticks} ticks.");

Console.Write("Vector: ");
ticks = InsertToEndTest(vector, itemsCount);
Console.WriteLine($"{ticks} ticks.");

Console.Write("Factor: ");
ticks = InsertToEndTest(factor, itemsCount);
Console.WriteLine($"{ticks} ticks.\r\n");


single = new SingleArray<int>();
vector = new VectorArray<int>();
factor = new FactorArray<int>();
Console.WriteLine("Вставка");
Console.WriteLine("---------------------------------------");

Console.Write("Single: ");
ticks = InsertTest(single, itemsCount);
Console.WriteLine($"{ticks} ticks.");

Console.Write("Vector: ");
ticks = InsertTest(vector, itemsCount);
Console.WriteLine($"{ticks} ticks.");

Console.Write("Factor: ");
ticks = InsertTest(factor, itemsCount);
Console.WriteLine($"{ticks} ticks.\r\n");



Console.WriteLine("Удаление");
Console.WriteLine("---------------------------------------");

Console.Write("Single: ");
ticks = RemoveTest(single, itemsCount);
Console.WriteLine($"{ticks} ticks.");

Console.Write("Vector: ");
ticks = RemoveTest(vector, itemsCount);
Console.WriteLine($"{ticks} ticks.");

Console.Write("Factor: ");
ticks = RemoveTest(factor, itemsCount);
Console.WriteLine($"{ticks} ticks.");



long InsertToEndTest(IDynamicArray<int> array, int itemsCount)
{
    var sw = Stopwatch.StartNew();
    for (int i = 0; i < itemsCount; i++)
        array.Add(i);

    sw.Stop();
    return sw.ElapsedTicks;
}

long InsertTest(IDynamicArray<int> array, int itemsCount)
{
    var sw = Stopwatch.StartNew();
    for (int i = 0; i < itemsCount; i++)
        array.Add(0, i);

    sw.Stop();
    return sw.ElapsedTicks;
}

long RemoveTest(IDynamicArray<int> array, int itemsCount)
{
    var sw = Stopwatch.StartNew();
    for (int i = 0; i < itemsCount; i++)
        array.Remove(0);

    sw.Stop();
    return sw.ElapsedTicks;
}
