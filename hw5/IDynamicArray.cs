﻿namespace hw5
{
    public interface IDynamicArray<T> where T : struct
    {
        void Add(T item);
        void Add(T item, int index);
        void Remove(int index);

        int Size { get; }
        T this[int index] { get; set; }
    }
}
