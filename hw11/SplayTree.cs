﻿//namespace hw11
//{
//    public class SplayTree : IBSTree
//    {
//        public void Insert(int key)
//        {
//            if (Root == null)
//            {
//                Root = new Node(key);
//                return;
//            }

//            Node currentItem = Root;
//            do
//            {
//                if (currentItem.Key == key) return;

//                if (currentItem.Key > key) //Left
//                {
//                    if (currentItem.Left == null) break;
//                    currentItem = currentItem.Left;
//                }
//                else //Right
//                {
//                    if (currentItem.Right == null) break;
//                    currentItem = currentItem.Right;
//                }
//            } while (true);

//            var newItem = new Node(key);

//            if (currentItem.Key > key)
//            {
//                currentItem.Left = newItem;
//            }
//            else
//            {
//                currentItem.Right = newItem;
//            }
//        }

//        private Node Splay(Node root, int key)
//        {
//            // Базовые случаи: root равен NULL или
//            // ключ находится в корне
//            if (root == null || root.Key == key)
//                return root;

//            // Ключ лежит в левом поддереве
//            if (root.Key > key)
//            {
//                // Ключа нет в дереве, мы закончили
//                if (root.Left == null) return root;

//                // Zig-Zig (Левый-левый) 
//                if (root.Left.Key > key)
//                {
//                    // Сначала рекурсивно поднимем
//                    // ключ как корень left-left
//                    root.Left.Left = Splay(root.Left.Left, key);

//                    // Первый разворот для root, 
//                    // второй разворот выполняется после else 
//                    root = RotateRight(root);
//                }
//                else if (root.Left.Key < key) // Zig-Zag (Левый-правый) 
//                {
//                    // Сначала рекурсивно поднимаем
//                    // ключ как корень left-right

//                    root.Left.Right = Splay(root.Left.Right, key);

//                    // Выполняем первый разворот для root.left
//                    if (root.Left.Right != null)
//                        root.Left = RotateLeft(root.Left);
//                }

//                // Выполняем второй разворот для корня
//                return (root.Left == null) ? root : RotateRight(root);
//            }
//            else // Ключ находится в правом поддереве 
//            {
//                // Ключа нет в дереве, мы закончили
//                if (root.Right == null) return root;

//                // Zag-Zig (Правый-левый) 
//                if (root.Right.Key > key)
//                {
//                    //Поднять ключ как корень right-left
//                    root.Right.Left = Splay(root.Right.Left, key);

//                    // Выполняем первый поворот для root.right
//                    if (root.Right.Left != null)
//                        root.Right = RotateRight(root.Right);
//                }
//                else if (root.Right.Key < key)// Zag-Zag (Правый-правый) 
//                {
//                    // Поднимаем ключ как корень 
//                    // right-right и выполняем первый разворот
//                    root.Right.Right = Splay(root.Right.Right, key);
//                    root = RotateLeft(root);
//                }

//                // Выполняем второй разворот для root
//                return (root.Right == null) ? root : RotateLeft(root);
//            }
//        }

//        private Node RotateRight(Node node)
//        {
//            Node y = node.Left;
//            node.Left = y.Right;
//            y.Right = node;
//            return y;
//        }

//        private Node RotateLeft(Node node)
//        {
//            Node y = node.Right;
//            node.Right = y.Left;
//            y.Left = node;
//            return y;
//        }

//        public void Remove(int key)
//        {
//            var item = SearchInternal(Root, key);
//            if (item == Root) throw new InvalidOperationException("You can't remove the root.");

//            if (item == null) return;

//            if (item.Left == null && item.Right == null) // it doesn't have children
//            {
//                RemoveEmpty(item);
//            }
//            else if ((item.Left != null && item.Right == null) || (item.Left == null && item.Right != null)) // it has the only one children
//            {
//                RemoveOnlyOne(item);
//            }
//            else if (item.Left != null && item.Right != null) // it has two children
//            {
//                RemoveTwoChildren(item);
//            }
//        }

//        private void RemoveEmpty(Node item)
//        {
//            var parent = item.Parent;
//            if (parent.Left == item)
//            {
//                parent.Left = null;
//            }
//            else if (parent.Right == item)
//            {
//                parent.Right = null;
//            }
//        }

//        private void RemoveOnlyOne(Node item)
//        {
//            var parent = item.Parent;
//            var itemToReplace = item.Right == null ? item.Left : item.Right;

//            if (parent.Left == item)
//            {
//                parent.Left = itemToReplace;
//            }
//            else if (parent.Right == item)
//            {
//                parent.Right = itemToReplace;
//            }
//        }

//        private void RemoveTwoChildren(Node item)
//        {
//            var itemToReplace = item.Right;
//            while (true)
//            {
//                if (itemToReplace.Left == null) break;
//                itemToReplace = itemToReplace.Left;
//            }

//            var tempKey = item.Key;
//            item.Key = itemToReplace.Key;
//            itemToReplace.Key = tempKey;

//            item.Parent.Left = null;
//            itemToReplace.Parent.Left = null;
//        }

//        public Node Search(int key)
//        {
//            Root = Splay(Root, key);
//            return Root;
//        }

//        private Node SearchInternal(Node item, int key)
//        {
//            if (item == null) return null;
//            if (key == item.Key) return item;

//            return key < item.Key
//                ? SearchInternal(item.Left, key)
//                : SearchInternal(item.Right, key);
//        }

//        public Node Root { get; private set; }
//    }
//}
