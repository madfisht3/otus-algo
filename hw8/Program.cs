﻿using hw8;
using System.Diagnostics;

TestSorting(100);
TestSorting(1000);
TestSorting(10000);
TestSorting(100000);
TestSorting(1000000);
Console.ReadLine();

void TestSorting(int arraySize)
{
    Console.WriteLine($"Array size = {arraySize}.");
    var testData1 = RandomDataGenerator.GenerateRandomArray(arraySize);
    var testData2 = testData1.ToArray(); // Cloning testData1 array
    var testData3 = testData1.ToArray(); // Cloning testData1 array

    var sw = Stopwatch.StartNew();
    testData1.QuickSort1();
    sw.Stop();

    // Testing whether or no sorting is correct.
    if (!testData1.ToArray().SequenceEqual(testData1.OrderBy(i => i))) throw new Exception("There's an error in the quick sorting #1 algorithm!");
    Console.WriteLine($"Quick sort #1:\t{sw.ElapsedTicks} ticks ~ {sw.ElapsedMilliseconds} ms");


    sw = Stopwatch.StartNew();
    testData2.QuickSort2();
    sw.Stop();

    // Testing whether or no sorting is correct.
    if (!testData2.ToArray().SequenceEqual(testData2.OrderBy(i => i))) throw new Exception("There's an error in the quick sorting #2 algorithm!");
    Console.WriteLine($"Quick sort #2:\t{sw.ElapsedTicks} ticks ~ {sw.ElapsedMilliseconds} ms");


    sw = Stopwatch.StartNew();
    testData3.MergeSort();
    sw.Stop();

    // Testing whether or no sorting is correct.
    if (!testData3.ToArray().SequenceEqual(testData3.OrderBy(i => i))) throw new Exception("There's an error in the merge sorting algorithm!");
    Console.WriteLine($"Merge sort:\t{sw.ElapsedTicks} ticks ~ {sw.ElapsedMilliseconds} ms");
    

    Console.WriteLine("----------------------------------------------");
}
