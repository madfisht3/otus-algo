﻿namespace hw8
{
    public static class ArraySortExtension
    {
        public static void QuickSort1<T>(this IList<T> collection)
            where T : struct, IComparable<T>
        {
            QuickSort1Internal(collection, 0, collection.Count - 1);
        }

        private static void QuickSort1Internal<T>(IList<T> collection,
            int leftIndex,
            int rightIndex) where T : struct, IComparable<T>
        {
            if (leftIndex < rightIndex)
            {
                int pt = Partitioning(collection, leftIndex, rightIndex);

                QuickSort1Internal(collection, leftIndex, pt - 1);
                QuickSort1Internal(collection, pt + 1, rightIndex);
            }
        }

        private static int Partitioning<T>(IList<T> collection,
            int leftIndex,
            int rightIndex) where T : struct, IComparable<T>
        {
            T pivot = collection[rightIndex];

            int m = leftIndex - 1;

            for (int j = leftIndex; j <= rightIndex; j++)
                if (collection[j].CompareTo(pivot) < 0) Swap(collection, ++m, j);

            Swap(collection, m + 1, rightIndex);
            return m + 1;
        }

        public static void MergeSort<T>(this IList<T> collection)
            where T : IComparable<T>
        {
            MergeSortInternal(collection, 0, collection.Count - 1);
        }

        private static void MergeSortInternal<T>(IList<T> collection,
            int index1, int index2)
            where T : IComparable<T>
        {
            if (index1 >= index2) return;
            int m = (index1 + index2) / 2;

            MergeSortInternal(collection, index1, m);
            MergeSortInternal(collection, m + 1, index2);
            Merge(collection, index1, m, index2);
        }

        private static void Merge<T>(IList<T> collection, int l, int x, int r) where T : IComparable<T>
        {
            T[] M = new T[r - l + 1];
            int a = l;
            int b = x + 1;
            int m = 0;

            while (a <= x && b <= r)
            {
                if (collection[a].CompareTo(collection[b]) < 0)
                    M[m++] = collection[a++];
                else
                    M[m++] = collection[b++];
            }

            while (a <= x)
            {
                M[m++] = collection[a++];
            }
            while (b <= r)
            {
                M[m++] = collection[b++];
            }

            for (int i = l; i <= r; i++)
            {
                collection[i] = M[i - l];
            }
        }

        public static void QuickSort2<T>(this IList<T> collection)
            where T : struct, IComparable<T>
        {
            QuickSort2Internal(collection, 0, collection.Count - 1);
        }

        private static void QuickSort2Internal<T>(IList<T> collection, 
            int leftIndex, int rightIndex) where T : struct, IComparable<T>
        {
            if (leftIndex >= rightIndex) return;

            (int i, int j) = Partition2(collection, leftIndex, rightIndex);

            QuickSort2Internal(collection, leftIndex, j);
            QuickSort2Internal(collection, i, rightIndex);
        }

        private static (int i, int j) Partition2<T>(IList<T> collection, 
            int leftIndex, int rightIndex) where T : struct, IComparable<T>
        {
            (int i, int j) = (leftIndex - 1, rightIndex);

            int p = leftIndex - 1;
            int q = rightIndex;
            var v = collection[rightIndex];

            while (true)
            {
                while (collection[++i].CompareTo(v) < 0) ;

                while (v.CompareTo(collection[--j]) < 0) if (j == leftIndex) break;

                if (i >= j) break;

                Swap(collection, i, j);

                if (collection[i].CompareTo(v) == 0)
                {
                    p++;
                    Swap(collection, p, i);
                }

                if (collection[j].CompareTo(v) == 0)
                {
                    q--;
                    Swap(collection, j, q);
                }
            }

            Swap(collection, i, rightIndex);
            j = i - 1;

            for (int k = leftIndex; k < p; k++, j--)
            {
                Swap(collection, k, j);
            }

            i++;
            for (int k = rightIndex - 1; k > q; k--, i++)
                Swap(collection, i, k);

            return (i, j);
        }

        private static void Swap<T>(this IList<T> collection, int index1, int index2)
        {
            (collection[index2], collection[index1]) = (collection[index1], collection[index2]);
        }
    }
}
